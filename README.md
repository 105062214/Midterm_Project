# Software Studio 2018 Spring Midterm Project 

## Topic
* 許願板
* Key functions (add/delete)
    1. wish list page
* Other functions (add/delete)
    1. 自訂username，在發布願望的時候如果有username，將會優先顯示username，而非email。
    2. you can delete post.

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description

一個簡單的許願板

使用者能夠選擇願望的類別並且留下你的願望

不同類別的願望前面會依照類別而有不同圖案以做區分

使用者也能刪除畫面上出現的願望

在左上角的account裡有個set username， 可以透過其設定自己的username

在許願當下，如果有設定username 則會在願望上顯示你的username，如果沒有將顯示email。

## Security Report (Optional)