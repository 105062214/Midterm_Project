function init() {
    var user_email = '';
    var user_name = '';
    var isLogin = false;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            
            user_email = user.email;
            isLogin = true;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='username-btn'>Set Username</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            userRef = firebase.database().ref("user_list/");
            userRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapShot){
                        var childData = childSnapShot.val();
                        if(childData.email == user_email){
                            user_name = childData.username;
                            
                        }
                    });
                })
                .catch(e => console.log(e.message));


            var btnUsername = document.getElementById('username-btn');
            var btnLogOut = document.getElementById('logout-btn');
            btnLogOut.addEventListener('click', function(e) {
                firebase.auth().signOut().then(
                    alert("logout successfully !"))
                .catch(error => alert(error.message));
            });




            var usertemp;
            btnUsername.addEventListener('click', function(e) {
                username = prompt("Please tell us what is your name.", "unlucky guy");
                if(username != ""){
                    userRef = firebase.database().ref("user_list/");
                    userRef.once('value')
                        .then(function (snapshot) {
                            snapshot.forEach(function (childSnapShot){
                                var childData = childSnapShot.val();
                                if(childData.email == user_email && childData.username != usertemp){
                                    oldUserRefString = "user_list/" + childData.username;
                                    oldUserRef = firebase.database().ref(oldUserRefString);
                                    oldUserRef.remove();
                                }
                            });
                        })
                        .catch(e => console.log(e.message));
                    firebase.database().ref("user_list/" + username).set({
                        username: username,
                        email: user_email
                    });
                    usertemp = username;
                    user_name = username;
                }
            })

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    wish_Type = document.getElementsByName('wishType');
    post_type = 'work';
    

    post_btn.addEventListener('click', function () {
        if(isLogin){
            if (post_txt.value != "") {
                /// TODO 6: Push the post to database's "com_list" node
                ///         1. Get the reference of "com_list"
                ///         2. Push user email and post data
                ///         3. Clear text field
                for (var i = 0; i < wish_Type.length; i++) {
                    if(wish_Type[i].checked){
                        post_type = wish_Type[i].value;
                    }
                }

                var newPostRef = firebase.database().ref("/com_list").push();
                var newPostkey = newPostRef.key
                newPostRef.set({
                    username: user_name,
                    email: user_email,
                    data: post_txt.value,
                    type: post_type,
                    key: newPostkey
                });
                post_txt.value = "";
            }
        }else{
            alert("You have to login first!");
        }
    });

    // The html code for post
    var str_before_postid = "<div class='my-3 p-3 bg-white rounded box-shadow'><button type='button' class='close' aria-label='Close' onclick='removePost(\"";
    var str_before_pic = "><span aria-hidden='true'>&times;</span></button><div><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6></div><div class='media text-muted pt-3'><img src='img/";
    var str_before_username = ".png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function (childSnapShot){
                var childData = childSnapShot.val();
                postInfo = childData.email
                if(childData.username){
                    postInfo = childData.username;
                }
                
                userRef = firebase.database().ref("user_list/");
                userRef.once('value')
                    .then(function (usersnapshot) {
                        usersnapshot.forEach(function (userchildSnapShot){
                            var userchildData = userchildSnapShot.val();
                            
                            if(userchildData.email == childData.email){
                                postInfo = userchildData.username;
                                
                            }
                            
                        });
                        
                    })
                    .catch(e => console.log(e.message));
                
                total_post[total_post.length] = str_before_postid + childData.key + "\")'" + str_before_pic + childData.type + str_before_username +  postInfo + "</strong>" + childData.data + str_after_content;
                first_count += 1;
               
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

           
            postsRef.on('child_added', function(data){
                second_count += 1;
                if(second_count > first_count){
                    var childData = data.val();
                    postInfo = childData.email
                    if(childData.username){
                        postInfo = childData.username;
                    }
                    
                    userRef = firebase.database().ref("user_list/");
                    userRef.once('value')
                        .then(function (usersnapshot) {
                            usersnapshot.forEach(function (userchildSnapShot){
                                var userchildData = userchildSnapShot.val();
                                if(userchildData.email == childData.email){
                                    postInfo = userchildData.username;
                                }
                                
                            });
                            
                        })
                        .catch(e => console.log(e.message));
                    
                    total_post[total_post.length] = str_before_postid + childData.key + "\")'" + str_before_pic + childData.type + str_before_username +  postInfo + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });

            postsRef.on('child_removed', function(){
                location.reload();
            })
        })
        .catch(e => console.log(e.message));

        
}

window.onload = function () {
    init();
};

var postInfo;

function removePost(postid){
    var sure = confirm("確認要刪除此則願望嗎?");
    if(sure == true){
        var refstring = 'com_list/' + postid;
        var postsRef = firebase.database().ref(refstring);
        console.log(postid);
        postsRef.remove();
    }
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}